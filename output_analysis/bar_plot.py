from operator import itemgetter
from collections import OrderedDict


__author__ = 'jnejati'

import numpy as np
import os
import matplotlib
import bench

matplotlib.use('svg')

import matplotlib.pyplot as plt


def read_file(f):
    table = [l.strip().split() for l in open(f).read().splitlines()]
    # table = np.asarray(table)
    return table


def do_plot_time(files_dict, param, page_type):  # data to plot

    label_list = []

    fig, ax = plt.subplots()
    plt.xlabel('Site')
    plt.ylabel('Time')
    #plt.title('PLT in Bandwidth=1Mbps, Delay=150ms' )
    plt.title('All network profile, mobile' )

    bar_width = 0.25
    opacity = 0.8
    list_plt_list = []
    for key, value in files_dict.items():
        plt_list = []
        for index, content in enumerate(value):
            my_index = np.arange(len(value))
            label_list.append(content[0])
            plt_list.append(content[1])
        print(key)
        print(len(np.array(plt_list).astype(float)))
        list_plt_list.append(plt_list)

    """d_rect1 = plt.bar(my_index, np.array(list_plt_list[0]).astype(float), bar_width,
            alpha=opacity,
            color='b',
            label='b1-d50')
    m_rect1 = plt.bar(my_index + bar_width, np.array(list_plt_list[1]).astype(float), bar_width,
            alpha=opacity,
            color='r',
            label='b1-d50')"""

    d_rect1 = plt.bar(my_index, np.array(list_plt_list[5]).astype(float), bar_width,
            alpha=opacity,
            color='g',
            label='b20-d5')

    d_rect2 = plt.bar(my_index, np.array(list_plt_list[4]).astype(float), bar_width,
            alpha=opacity,
            color='mediumseagreen',
            label='b20-d50',
            bottom=np.array(list_plt_list[5]).astype(float))

    d_rect3 = plt.bar(my_index, np.array(list_plt_list[3]).astype(float), bar_width,
            alpha=opacity,
            color='lightgreen',
            label='b20-d150',
            bottom=np.array(list_plt_list[4]).astype(float) + np.array(list_plt_list[5]).astype(float))

    d_rect4 = plt.bar(my_index, np.array(list_plt_list[8]).astype(float), bar_width,
            alpha=opacity,
            color='navy',
            label='b5-d5',
            bottom=np.array(list_plt_list[3]).astype(float) + np.array(list_plt_list[4]).astype(float) + np.array(list_plt_list[5]).astype(float))

    d_rect5 = plt.bar(my_index, np.array(list_plt_list[7]).astype(float), bar_width,
            alpha=opacity,
            color='b',
            label='b5-d50',
            bottom=np.array(list_plt_list[3]).astype(float) + np.array(list_plt_list[4]).astype(float) + np.array(list_plt_list[5]).astype(float) + np.array(list_plt_list[8]).astype(float))

    d_rect6 = plt.bar(my_index, np.array(list_plt_list[6]).astype(float), bar_width,
            alpha=opacity,
            color='royalblue',
            label='b5-d150',
            bottom=np.array(list_plt_list[3]).astype(float) + np.array(list_plt_list[4]).astype(float) + np.array(list_plt_list[5]).astype(float) + np.array(list_plt_list[7]).astype(float) + + np.array(list_plt_list[8]).astype(float) )


    d_rect7 = plt.bar(my_index, np.array(list_plt_list[2]).astype(float), bar_width,
            alpha=opacity,
            color='r',
            label='b1-d5',
            bottom=np.array(list_plt_list[6]).astype(float) + np.array(list_plt_list[7]).astype(float) + np.array(list_plt_list[8]).astype(float) + np.array(list_plt_list[3]).astype(float) + np.array(list_plt_list[4]).astype(float)+ np.array(list_plt_list[5]).astype(float))

    d_rect8 = plt.bar(my_index, np.array(list_plt_list[1]).astype(float), bar_width,
            alpha=opacity,
            color='salmon',
            label='b1-d50',
            bottom=np.array(list_plt_list[6]).astype(float) + np.array(list_plt_list[7]).astype(float) + np.array(list_plt_list[8]).astype(float) + np.array(list_plt_list[3]).astype(float) + np.array(list_plt_list[4]).astype(float)+ np.array(list_plt_list[5]).astype(float) + np.array(list_plt_list[2]).astype(float))

    d_rect9 = plt.bar(my_index, np.array(list_plt_list[0]).astype(float), bar_width,
            alpha=opacity,
            color='y',
            label='b1-d150',
            bottom=np.array(list_plt_list[6]).astype(float) + np.array(list_plt_list[7]).astype(float) + np.array(list_plt_list[8]).astype(float) + np.array(list_plt_list[3]).astype(float) + np.array(list_plt_list[4]).astype(float)+ np.array(list_plt_list[5]).astype(float) + np.array(list_plt_list[1]).astype(float) + np.array(list_plt_list[2]).astype(float))


    ### Mobile
    """m_rect1 = plt.bar(my_index + bar_width, np.array(list_plt_list[14]).astype(float), bar_width,
            alpha=opacity,
            color='g',
            label='b20-d5')

    m_rect2 = plt.bar(my_index + bar_width, np.array(list_plt_list[13]).astype(float), bar_width,
            alpha=opacity,
            color='mediumseagreen',
            label='b20-d50',
            bottom=np.array(list_plt_list[14]).astype(float))

    m_rect3 = plt.bar(my_index + bar_width, np.array(list_plt_list[12]).astype(float), bar_width,
            alpha=opacity,
            color='lightgreen',
            label='b20-d150',
            bottom=np.array(list_plt_list[13]).astype(float) + np.array(list_plt_list[14]).astype(float))

    m_rect4 = plt.bar(my_index + bar_width, np.array(list_plt_list[17]).astype(float), bar_width,
            alpha=opacity,
            color='navy',
            label='b5-d5',
            bottom=np.array(list_plt_list[12]).astype(float) + np.array(list_plt_list[13]).astype(float) + np.array(list_plt_list[14]).astype(float))

    m_rect5 = plt.bar(my_index + bar_width, np.array(list_plt_list[16]).astype(float), bar_width,
            alpha=opacity,
            color='b',
            label='b5-d50',
            bottom=np.array(list_plt_list[12]).astype(float) + np.array(list_plt_list[13]).astype(float) + np.array(list_plt_list[14]).astype(float) + np.array(list_plt_list[17]).astype(float))

    m_rect6 = plt.bar(my_index + bar_width, np.array(list_plt_list[15]).astype(float), bar_width,
            alpha=opacity,
            color='royalblue',
            label='b5-d150',
            bottom=np.array(list_plt_list[12]).astype(float) + np.array(list_plt_list[13]).astype(float) + np.array(list_plt_list[14]).astype(float) + np.array(list_plt_list[16]).astype(float) + + np.array(list_plt_list[17]).astype(float) )


    m_rect7 = plt.bar(my_index + bar_width, np.array(list_plt_list[11]).astype(float), bar_width,
            alpha=opacity,
            color='r',
            label='b1-d5',
            bottom=np.array(list_plt_list[15]).astype(float) + np.array(list_plt_list[16]).astype(float) + np.array(list_plt_list[17]).astype(float) + np.array(list_plt_list[12]).astype(float) + np.array(list_plt_list[13]).astype(float)+ np.array(list_plt_list[14]).astype(float))

    m_rect8 = plt.bar(my_index + bar_width, np.array(list_plt_list[10]).astype(float), bar_width,
            alpha=opacity,
            color='salmon',
            label='b1-d50',
            bottom=np.array(list_plt_list[15]).astype(float) + np.array(list_plt_list[16]).astype(float) + np.array(list_plt_list[17]).astype(float) + np.array(list_plt_list[12]).astype(float) + np.array(list_plt_list[13]).astype(float)+ np.array(list_plt_list[14]).astype(float) + np.array(list_plt_list[11]).astype(float))

    m_rect9 = plt.bar(my_index + bar_width, np.array(list_plt_list[9]).astype(float), bar_width,
            alpha=opacity,
            color='y',
            label='b1-d150',
            bottom=np.array(list_plt_list[15]).astype(float) + np.array(list_plt_list[16]).astype(float) + np.array(list_plt_list[17]).astype(float) + np.array(list_plt_list[12]).astype(float) + np.array(list_plt_list[13]).astype(float)+ np.array(list_plt_list[14]).astype(float) + np.array(list_plt_list[10]).astype(float) + np.array(list_plt_list[11]).astype(float))
    """

    label_tuple = tuple(sorted(list(set(label_list))))
    # print(label_tuple)

    plt.xticks(my_index, label_tuple, fontsize=6, rotation=90)
    plt.tight_layout()
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),loc='best', shadow=True, fontsize='8')
    #plt.legend(loc='best', shadow=True, fontsize='4')

    def autolabel(rects):
        for rect in rects:
            h = rect.get_height()
            ax.text(rect.get_x()+rect.get_width()/2., 1.05*h, '%d'%int(h),
                    ha='center', va='center', rotation=90, fontsize=2)
    plt.show()
    plt.savefig('../plots/'  + "All_net_bar_plot_mobile.pdf")
    plt.close()


def main():
    #top200_list = ['ft', 'bing', 'deseret', 'blogspot',  'google', 'live.com', 'theguardian', 'seattletimes', 'onclickads', 'weibo.com', 'wikipedia', 'wsj']
    top200_list =[]
    top200_data = []
    heavy_data = []
    path = '../OutputFiles'
    dirs = os.listdir(path)
    dict_plt_top200 = OrderedDict()
    dict_plt_heavy = OrderedDict()
    my_intersect = bench.intersect_sites()
    for files in dirs:
        if files.endswith('.bench'):
            files = os.path.join(path, files)
            table_1 = read_file(files)
            a = [table_1[i][0:5:4] for i in range(len(table_1)) if table_1[i][0] in my_intersect]
            a = sorted(a, key=itemgetter(0))
            top200_data = [x for x in a if x[0].startswith(tuple(top200_list))]
            heavy_data = [x for x in a if not x[0].startswith(tuple(top200_list))]
            print(top200_data)
            print(heavy_data)
            name = "_".join(files.split('/')[2].split('_')[:2])
            #print(name)

            dict_plt_top200[name] = top200_data
            dict_plt_heavy[name] = heavy_data
    #print(dict_plt_top200)
    print(dict_plt_heavy)
    #do_plot_time(dict_plt_top200, 'plt', 'top200')
    do_plot_time(dict_plt_heavy, 'plt', 'heavy')

    #do_plot_time(dict_plt, 'plt', 'heavy')



if __name__ == "__main__":
    main()