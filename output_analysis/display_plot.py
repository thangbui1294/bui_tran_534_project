__author__ = 'jnejati'

import os
from os import path as p
import numpy as np
import matplotlib
matplotlib.use('svg')
import matplotlib.pyplot as plt


def read_file(f):
    table = [l.strip().split() for l in open(f).read().splitlines()]
    table = np.asarray(table)
    return table


def make_cdf(values, key):
    cdfvalues = []
    cdffile = open("../OutputFiles/CDF/" + key + "_cdf.txt", "w")
    index = 1
    for value in values:
        cdfvalue = index / (len(values) * 1.0)
        cdfvalues.append(cdfvalue)
        cdffile.write(str(value) + "\t" + str(cdfvalue) + "\n")
        index += 1
    return cdffile.name

def do_plot_time(file1, file2):
    t1 = [l.strip().split() for l in open(file1).read().splitlines()]
    t2 = [l.strip().split() for l in open(file2).read().splitlines()]
    t1 = np.asarray(t1).astype(float)
    t2 = np.asarray(t2).astype(float)

    fig = plt.figure()
    ax2 = fig.add_subplot(1, 1, 1)
    fig.set_size_inches((7, 5))
    line_1, line_2 = ax2.plot(t1[:, 0], t1[:, 1], 'b', \
                              t2[:, 0], t2[:, 1], 'r', \
                              linewidth=2.0)
    ax2.tick_params(axis='x')
    ax2.tick_params(axis='y')
    ax2.grid(True)
    ax2.set_ylim([0, 1])
    ax2.set_xlim([0, 20000])
    ax2.set_title(file1.split('/')[-1] + ' vs ' + file2.split('/')[-1])
    ax2.set_xlabel('Time')
    ax2.set_ylabel('CDF')
    line_1.set_label(file1.split('/')[-1].split('.')[0])
    line_2.set_label(file2.split('/')[-1].split('.')[0])
    legend = plt.legend(loc='best', shadow=True, fontsize='medium')
    #plt.show()
    plt.savefig('../plots/' + file1.split('/')[-1].split('.')[0] + ' vs ' + file2.split('/')[-1].split('.')[0] + ".pdf")
    plt.close()


def main():
    path = '../OutputFiles'
    dirs = os.listdir(path)
    for files in dirs:
        if files.endswith('.bench'):
            files = os.path.join(path, files)
            table_1 = read_file(files)
            print(files)
            #print(table_1)
            #data/laptop_eth/desktop_pages_content_heavy/temp_files/wprof_300_5_pro_1/
            #data/mobile_wifi/mobile_pages_content_heavy_cold/temp_files/wprof_300_5_pro_1/
            name1 = table_1[0, 1].split("/")[1] + '_' + table_1[0, 1].split("/")[2]
            name2 = table_1[0, 5].split("/")[1] + '_' + table_1[0, 5].split("/")[2]
            file1 = make_cdf(sorted(np.array(table_1[:, 2]).astype(float).tolist()), 'comp_' + name1)
            file2 = make_cdf(sorted(np.array(table_1[:, 6]).astype(float).tolist()), 'comp_' + name2)
            do_plot_time(file1, file2)
            file1 = make_cdf(sorted(np.array(table_1[:, 3]).astype(float).tolist()), 'net_' + name1)
            file2 = make_cdf(sorted(np.array(table_1[:, 7]).astype(float).tolist()), 'net_' + name2)
            do_plot_time(file1, file2)
            file1 = make_cdf(sorted(np.array(table_1[:, 4]).astype(float).tolist()), 'plt_' + name1)
            file2 = make_cdf(sorted(np.array(table_1[:, 8]).astype(float).tolist()), 'plt_' + name2)
            do_plot_time(file1, file2)

if __name__ == "__main__":
    main()
