from operator import itemgetter

__author__ = 'jnejati'
import tldextract
import os
import numpy as np
import copy
from functools import reduce



def display_output(in_dict, name):

    f = open('../OutputFiles/' + name, 'w+')
    for key, value in in_dict.items():
        f.write(key + '\t' + str(value[0]['dir']) + '\t' + str(value[0]['comp']) + '\t' + str(value[0]['network']) + '\t' + str(value[0]['plt']) + '\n')
    """for key, value in in_dict.items():
        f.write(key + '\t'  + str(value[0]['comp']) + '\t' + str(value[0]['network']) + '\t' + str(value[0]['plt']) + \
                '\t' + str(value[1]['comp']) + '\t' + str(value[1]['network']) + '\t' + str(value[1]['plt']) + '\n')
    for key, value in in_dict.items():
        f.write(key + '\t'  +  str(value[0]['plt']) + \
                '\t' + str(value[1]['plt']) + '\n') """


def plt_files(dir):
    site_stats = {}
    files_dir = os.listdir(dir)
    for file1 in files_dir:
        if file1.startswith('original.testbed.localhost'):
            ext1 = tldextract.extract(file1.split('original.testbed.localhost_')[1])
            print(file1.split('original.testbed.localhost_')[1])
            tld1 = ext1.domain + '.' + ext1.suffix
            file1 = os.path.join(dir, file1)
            file1_dict = grab_data(file1)
            dict_list = []
            if file1_dict:
                file1_dict['dir'] = dir
                dict_list.append(file1_dict)
                site_stats[tld1]= copy.deepcopy(dict_list)
    display_output(site_stats, dir.split('/')[2] + '.bench')
    return site_stats

def read_file(f):
    table = [l.strip().split() for l in open(f).read().splitlines()]
    # table = np.asarray(table)
    return table


def intersect_sites():

    path = '../OutputFiles'
    dirs = os.listdir(path)
    sitename_set_list = []
    for files in dirs:
        if files.endswith('.bench'):
            files = os.path.join(path, files)
            table_1 = read_file(files)
            a = [table_1[i][0] for i in range(len(table_1))]
            a = sorted(a, key=itemgetter(0))
            a = set(a)
            print('set a: ', a)
            sitename_set_list.append(a)

    return set.intersection(*sitename_set_list)


"""def temp_files_intersect(dir_orig, dir_modified):
    site_stats = {}
    files_dir1 = os.listdir(dir_orig)
    files_dir2 = os.listdir(dir_modified)
    for file1 in files_dir1:
        if file1.startswith('original.testbed.localhost'):
            ext1 = tldextract.extract(file1.split('original.testbed.localhost_')[1])
            print(file1.split('original.testbed.localhost_')[1])
            tld1 = ext1.domain + '.' + ext1.suffix
            file1 = os.path.join(dir_orig, file1)
            for file2 in files_dir2:
                if file2.startswith('original.testbed.localhost'):
                    ext2 = tldextract.extract(file2.split('original.testbed.localhost_')[1])
                    print(file2.split('original.testbed.localhost_')[1])
                    tld2 = ext2.domain + '.' + ext2.suffix
                    file2 = os.path.join(dir_modified, file2)
                    if tld1 == tld2:
                        file1_dict = grab_data(file1)
                        file2_dict = grab_data(file2)
                        if file1_dict and file2_dict:
                            dict_list = []
                            file1_dict['dir'] = dir_orig
                            file2_dict['dir'] = dir_modified
                            dict_list.append(file1_dict)
                            dict_list.append(file2_dict)
                            site_stats[tld1]= copy.deepcopy(dict_list)
    display_output(site_stats, dir_orig.split('/')[2] + '_and_' + dir_modified.split('/')[2] + '.bench')
    return site_stats"""


def grab_data(in_file):
    file = open(in_file, 'r')
    time_dict = {}
    network = 0
    computation = 0
    load = 0
    for line in file:
        list1 = line.split(':')
        if str(list1[0]) == "time_download":
            network = float(list1[1])
            time_dict['network'] = network
        if str(list1[0]) == "time_comp":
            computation = float(list1[1])
            time_dict['comp'] = computation
        if str(list1[0]) == "load":
            load = float(list1[1])
            time_dict['plt'] = load
    if network == 0 or computation == 0 or not (abs(load - (network + computation)) < 1): return None
    return time_dict

def main():
    #print(os.getcwd())
    dirs_list= []
    """dirs_list.append('../data/desktop_wifi-b1-d5_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b1-d50_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b1-d150_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b5-d5_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b5-d50_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b5-d150_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b20-d5_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b20-d50_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/desktop_wifi-b20-d150_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')"""

    dirs_list.append('../data/mobile_wifi-b1-d5_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b1-d50_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b1-d150_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b5-d5_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b5-d50_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b5-d150_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b20-d5_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b20-d50_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')
    dirs_list.append('../data/mobile_wifi-b20-d150_mixed200_orig_minification/temp_files/wprof_300_5_pro_1/')

    for my_dir in dirs_list:
        print(my_dir)
        plt_files(my_dir)
    print('intersect:', intersect_sites())



    ### Desktop high bandwidth
    """temp_files_intersect(dirs_list[1], dirs_list[1])
    temp_files_intersect(dirs_list[0], dirs_list[1])"""

    ### Mobile high bandwidth
    """temp_files_intersect(dirs_list[5], dirs_list[5])
    temp_files_intersect(dirs_list[4], dirs_list[5])"""

    #Desktop low bandwidth
    """temp_files_intersect(dirs_list[3], dirs_list[3])
    temp_files_intersect(dirs_list[2], dirs_list[3])"""

    #Mobile low bandwidth
    """temp_files_intersect(dirs_list[7], dirs_list[7])
    temp_files_intersect(dirs_list[6], dirs_list[7])"""
     #------------------------------------------------------------------
    #Desktop low delay
    """temp_files_intersect(dirs_list[1], dirs_list[1])
    temp_files_intersect(dirs_list[3], dirs_list[1])"""
    #Mobile low delay
    """temp_files_intersect(dirs_list[5], dirs_list[5])
    temp_files_intersect(dirs_list[7], dirs_list[5])"""

    #Desktop high delay
    """temp_files_intersect(dirs_list[0], dirs_list[0])
    temp_files_intersect(dirs_list[2], dirs_list[0])"""

    #Mobile high delay
    """temp_files_intersect(dirs_list[4], dirs_list[4])
    temp_files_intersect(dirs_list[6], dirs_list[4])"""


    # All Mobiles
    """temp_files_intersect(dirs_list[9], dirs_list[12])
    temp_files_intersect(dirs_list[10], dirs_list[12])
    temp_files_intersect(dirs_list[11], dirs_list[12])
    temp_files_intersect(dirs_list[12], dirs_list[12])
    temp_files_intersect(dirs_list[13], dirs_list[12])
    temp_files_intersect(dirs_list[14], dirs_list[12])
    temp_files_intersect(dirs_list[15], dirs_list[12])
    temp_files_intersect(dirs_list[16], dirs_list[12])
    temp_files_intersect(dirs_list[17], dirs_list[12])"""

    # All Desktops
    """temp_files_intersect(dirs_list[0], dirs_list[3])
    temp_files_intersect(dirs_list[1], dirs_list[3])
    temp_files_intersect(dirs_list[2], dirs_list[3])
    temp_files_intersect(dirs_list[3], dirs_list[4])
    temp_files_intersect(dirs_list[4], dirs_list[3])
    temp_files_intersect(dirs_list[5], dirs_list[3])
    temp_files_intersect(dirs_list[6], dirs_list[3])
    temp_files_intersect(dirs_list[7], dirs_list[3])
    temp_files_intersect(dirs_list[8], dirs_list[3])"""




if __name__ == "__main__":
    main()


