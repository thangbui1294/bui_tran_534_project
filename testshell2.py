__author__ = 'jnejati'
import sys
import subprocess
import  time

sys.path.append('/home/jnejati/android-sdk-linux/tools/lib')
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice

# Connects to the current device, returning a MonkeyDevice object
device = MonkeyRunner.waitForConnection()

# sets a variable with the package's internal name
package = 'org.chromium.chrome.testshell'

# sets a variable with the name of an Activity in the package
activity = 'org.chromium.chrome.testshell.ChromiumTestShellActivity'

#Uninstall the app
device.removePackage(package)

# Installs the Android package. Notice that this method returns a boolean, so you can test
# to see if the installation worked.
device.installPackage('/home/jnejati/chrome/ChromiumTestShell.apk')

# sets the name of the component to start
runComponent = package + '/' + activity

# Runs the component
device.startActivity(component=runComponent)
time.sleep(5)
device.shell('am start -n org.chromium.chrome.testshell/org.chromium.chrome.testshell.ChromiumTestShellActivity http://original.testbed.localhost/www.facebook.com/')
time.sleep(50)
#print('Activity loaded')
