__author__ = 'jnejati'

import experiments
import convert
import post_analyze
import apache_conf
import network_emulator
from urllib.parse import urlparse
import  chromium_driverpx
import time
import modifications as modify
from bs4 import BeautifulSoup
import urllib.request
import urllib.response
import io
import gzip
import subprocess


def main():
    input_file = 'new100.txt'
    #exp_type = 'compression'
    #exp_type = 'minification'
    exp_type = 'inline'
    arch_dir = '/home/jnejati/page_speed/arch_dir'
    my_profiles = [
                   # Wi-Fi ac
                  {'conn_type':'wifi-fast',
                  'device_type': 'desktop',
                  'page_type': 'pxc_d10',
                  'cache': 'yes',
                  'download_rate':'100Mbit',
                  'download_delay':'10ms',
                  'download_loss':'0.1%',
                  'upload_rate':'90Mbit',
                  'upload_delay':'10ms',
                  'upload_loss':'0.1%'}]

    #apache_conf.rm_dir(arch_dir)
    #apache_conf.fetch_all_sites(input_file, arch_dir)    
    for run_no in range(1):
        top_sites = (l.strip().split() for l in open("./res/" + input_file).read().splitlines())
        count = 0
        for site in top_sites:
            # n sites only
            if count not in range(100):
                break
            print('\n\n\n================== ANALYZING NEW SITE =========================')
            print(site[0])
            print('================== ANALYZING NEW SITE =========================\n\n\n')
            
            for net_profile in my_profiles:
                print('Current profile: ' + net_profile['cache'] + ' - ' + net_profile['download_delay'])
                s1 = urlparse(site[0])
                apache_conf.rm_dir('/var/www/original.testbed.localhost')
                command = ["cp", "-R", arch_dir + '/' + s1.netloc + '/.', '/var/www/original.testbed.localhost']
                try:
                    proc = subprocess.call(command, shell=False, timeout=30)
                    print(s1.netloc + ' copied')
                except subprocess.TimeoutExpired:
                    print("Couldn't copy ", s1.netloc)

                apachectl_orig = apache_conf.ApacheConf(net_profile['device_type'], net_profile['conn_type'], input_file.split('.')[0], 'orig', '/home/jnejati')
                apachectl_orig.a2ensite('original.testbed.localhost')
                apache_conf.ApacheConf.restart_apache()
                apachectl_orig.initialize_archive_dir()

                try:
                    print("Run Network Emulator")
                    netns = network_emulator.NetworkEmulator(net_profile)
                    netns.set_profile(net_profile['conn_type'])

                    # run 1: 
                    print("Run Chromium")
                    my_run = chromium_driverpx.RunChromium(site[0], 'orig',  net_profile)
                    my_run.main_run()

                    # run 2: if use cache
                    if net_profile['cache'] is 'yes':
                        print('sleep 3 seconds between two load')
                        time.sleep(3);
                        my_run = chromium_driverpx.RunChromium(site[0], 'orig',  net_profile)
                        my_run.main_run()
                    print("Converting and analyzing")
                    conv_orig = convert.Convert()
                    if conv_orig.do_analysis(net_profile, 'orig', exp_type):
                        print("Origin json file analyzed")
                    
                except:
                    print("dkmm")

            count = count + 1
        

if __name__ == '__main__':
    main()
